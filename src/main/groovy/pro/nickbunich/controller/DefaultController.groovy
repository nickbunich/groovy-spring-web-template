package pro.nickbunich.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController("/")
class DefaultController {

	@GetMapping
	def get() {
		[foo: 1, bar: "baz", quux: 3.14]
	}

}
